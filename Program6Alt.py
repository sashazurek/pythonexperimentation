import graphics

def main():
    # Using the graphics functions Point, Line, Circle, Rectangle, and Oval
    # Make a fall-related art piece
    # At least one of the circles, rectangles, or ovals must use the "fill"
    fallArt = graphics.GraphWin("Art",400,400)
    moonPoint = graphics.Point(200,250)
    pumpkinBody = graphics.Circle(moonPoint,150)
    pumpkinBody.setFill("orange")
    pumpkinBody.setOutline("orange")
    pumpkinBody.draw(fallArt)
    moonEyePoint1 = graphics.Point(150,150)
    moonEyePoint2 = graphics.Point(175,200)
    moonEye1 = graphics.Oval(moonEyePoint1,moonEyePoint2)
    moonEye1.setFill("brown")
    moonEye1.setOutline("brown")
    moonEye1.draw(fallArt)
    moonEyePoint3 = graphics.Point(225,150)
    moonEyePoint4 = graphics.Point(250,200)
    moonEye2 = graphics.Oval(moonEyePoint3,moonEyePoint4)
    moonEye2.setFill("brown")
    moonEye2.setOutline("brown")
    moonEye2.draw(fallArt)
    nosePoint1 = graphics.Point(200,210)
    nosePoint2 = graphics.Point(165,250)
    nosePoint3 = graphics.Point(235,250)
    noseTriangle = graphics.Polygon(nosePoint1,nosePoint2,nosePoint3)
    noseTriangle.setFill("brown")
    noseTriangle.setOutline("brown")
    noseTriangle.draw(fallArt)
    mouthPoint1 = graphics.Point(100,300)
    mouthPoint2 = graphics.Point(300,325)
    mouthRectangle = graphics.Rectangle(mouthPoint1,mouthPoint2)
    mouthRectangle.setFill("brown")
    mouthRectangle.setOutline("brown")
    mouthRectangle.draw(fallArt)
    eyebrowPoint1 = graphics.Point(135,135)
    eyebrowPoint2 = graphics.Point(265,135)
    eyebrow = graphics.Line(eyebrowPoint1,eyebrowPoint2)
    eyebrow.setOutline("brown")
    eyebrow.draw(fallArt)
    hatPoint1 = graphics.Point(100,100) # 1
    hatPoint2 = graphics.Point(300,100) # 2
    hatPoint8 = graphics.Point(100,75) # 8
    hatPoint3 = graphics.Point(300,75) # 3
    hatPoint7 = graphics.Point(150,75) # 7
    hatPoint4 = graphics.Point(250,75) # 4
    hatPoint6 = graphics.Point(150,10) # 6
    hatPoint5 = graphics.Point(250,10) # 5
    pumpkinHat = graphics.Polygon(hatPoint1,hatPoint2,hatPoint3,
                                  hatPoint4,hatPoint5,hatPoint6,
                                  hatPoint7,hatPoint8)
    pumpkinHat.setFill("brown")
    pumpkinHat.setOutline("brown")
    pumpkinHat.draw(fallArt)
    return

main()
