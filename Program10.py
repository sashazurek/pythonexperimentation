import math

# Take a list of sanitized numbers for the Sieve
def sieve(numList,upper):
    # For each number in the list until sqrt(upper)+1
    for i in range(3,int(math.sqrt(upper)+1),2):
        # Iterate over the list and remove any number
            # of which the announced is a multiple of
        for mult in range(i,(upper+1),i):
            # if mult and i are equivalent, do nothing.
            if mult == i:
                pass
            # If the item in the list is divisble by i,
                # remove it.
            elif mult in numList:
                numList.remove(mult)
    return numList

def main():
    # Take an upper range from the user
    upper = int(input("Give the upper bound: "))
    # If the user has put in a not-in-range number, tell them and stop.
    if upper <= 1:
        print("Error: Input is less than 2. This does not work.")
        return
    # Print all potentially-prime numbers into a list
    primeList = [2]
    for i in range(3,(upper+1),2):
        primeList.append(i)
    # Send the list into the Sieve
    print(primeList)
    primeList = sieve(primeList,upper)
    for item in primeList:
        print(item)
    return

main()
