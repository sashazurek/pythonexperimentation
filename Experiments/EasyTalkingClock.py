# Challenge: Easy Talking Clock
# Input: An hour (0-23) followed by a colon followed by the minute (0-59).
# Output: The time in words, using 12-hour format, followed by am or pm.
# Sample input: 00:00, 01:30, 20:29, 21:00
# Sample output: It's twelve am, It's one thirty am, It's eight twenty nine pm, It's nine pm
# Challenge #321 from r/dailyprogrammer, posted 2017-06-27
# ALGORITHM
## Ask user for input, in 24 hour format
## Take the hour and the minute, and put them in their own variables.
### Take the hours, and convert them to 12 hour format.
#### Store the numerical value and day/night
### Print a modifiable sentence, in the format "It's time xm"
# Take inputTime from main()
# Return a 12-hour format time

def hour(time):
    # Convert the hours string to an integer
    time = int(time[0:2])
    # Check for invalid input, check if 24 hour notation
    if time >= 24:
        print("Invalid input!")
        main()
    elif time > 12:
        time = time - 12
    # Take the 12 hour notation and convert it to English text
    hours = ["","one","two","three","four,","five",
    "six","seven","eight","nine","ten","eleven","twelve"]
    return hours.pop(time)

def amPM(time):
    # Take the input hours, and turn them into a number
    time = int(time[0:2])
    # Catch an error when input hours is above 24
    if time >= 24:
        print("Invalid input!")
        main()
    # Return pm if hours are 12-23
    elif time >= 12:
        return "pm"
    # Otherwise return am
    else:
        return "am"

def minute(time):
    # Look at the value of the minutes, and check if it fits one of these categories:
        # 1. Is a number with a single-word expression (0-20)
        # 2. Is a double-digit number with a multi-word expression (21-59)
    minutes = int(time[3:5])
    if minutes >=21:
        # Separate the tens place and the ones place into their own integer values
        # Then assign each value its respective English expression
        tens = int(time[3:4])
        ones = int(time[4:5])
        # tensTime contains dummy values to avoid arithmetic.
        tensTime = ["ah","no","twenty","thirty","fourty","fifty"]
        onesTime = ["","one","two","three","four,","five",
        "six","seven","eight","nine"]
        return tensTime.pop(tens) + "-" + onesTime.pop(ones)
    else:
        # Assign the number its respective English Expression
        theTime = ["","oh one","oh two","oh three","oh four","oh five",
        "oh six","oh seven","oh eight","oh nine","ten","eleven","twelve",
        "thirteen","fourteen","fifteen","sixteen","seventeen",
        "eighteen","nineteen","twenty"]
        return theTime.pop(minutes)
   

def main():
    inputTime = str(input("Tell me the time, in 24 hour format (xx:xx) "))
    print("It's",hour(inputTime),minute(inputTime),amPM(inputTime))
    return

main()