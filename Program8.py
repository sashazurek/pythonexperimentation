def subEncrypt(clearText, key):
    # Define the alphabet to use
    alphabet = 'abcdefghijklmnopqrstuvwxyz '
    # The variable to build the encrypted text letter-by-letter
    encryptText = ''
    # Process each character in the cleartext if it is an alpha or a space
    # Pass everything else through untouched
    for ch in clearText:
        ind = getIndex(ch,alphabet)
        if ind == -1:
            encryptText = encryptText + ch
        else:
            encChar = key[ind]
            encryptText = encryptText + encChar
    return encryptText

def subDecrypt(encryptText,key):
    # Define the alphabet to use
    alphabet = 'abcdefghijklmnopqrstuvwxyz '
    # The variable to build the decrypted text letter-by-letter
    clearText = ''
    # Process each character in the decryptText if it is an alpha or space
    # Pass everything else through untouched
    for ch in encryptText:
        ind = getIndex(ch,key)
        if ind == -1:
            clearText = clearText + ch
        else:
            clearChar = alphabet[ind]
            clearText = clearText + clearChar
    return clearText

def getIndex(theChar,theStr):
    return theStr.find(theChar)

def main():
    myKey = 'pyfgcrlaoeuid htnsqjkxbmwvz'
    # Get input and lower it using the message in myMessage
    myMessage = input("Give me a message to encrypt. ")
    myMessage = myMessage.lower()
    # Encrypt the message using the input text and myKey
    encrypted = subEncrypt(myMessage,myKey)
    # Print the encrypted text
    print(encrypted)
    # Decrypt the message
    decrypted = subDecrypt(encrypted,myKey)
    # Print the decrypted message
    print(decrypted)
    return

main()
