def main():
    dist1Miles, dist2Miles = eval(input("Enter two distances separated by a comma. "))
    dist1Rods = dist1Miles * 320
    dist2Rods = dist2Miles * 320
    print(dist1Miles , "miles is" , dist1Rods , "rods." ,  dist2Miles ,  "miles is" ,  dist2Rods , "rods.")
    areaRods = dist1Rods * dist2Rods
    print("The area of a rectangle, with sides of" , dist1Rods , "rods and" , dist2Rods , "rods is" ,  areaRods , "rods squared. ")

    return

main()
