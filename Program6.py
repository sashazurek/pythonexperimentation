from graphics import *

# barChart takes origin, maximum X, maximum Y, a set of values, and a window
def barChart(origin, maxX, maxY, values, theWin):
    # Draw the X and Y axis
    # Calculate the height of the tallest bar
    # Calculate the width of each bar
    # For each value, draw the corresponding bar
        # Assume that the values are listed in left-to-right order
        # Calculate the height of the current bar
        # Calculate the lower left point of the bar
        # Create the needed objects
        # Draw the bar
    return


def main():
    # Ask the user for each of the values we need for barChart()
    origin = input("Please enter the origin for your graph. ")
    maxX = input("Please enter the maximum X value for your graph. ")
    maxY = input("Please enter the maximum Y value for your graph. ")
    return

main()